﻿using Data;
using System;

namespace Logic
{

    public class Play : IPlay
    {
        public IBoard board { get; set; }
        public IRobot robot { get; set; }

        public Play(IRobot _robot, IBoard _board)
        {
            board = _board;
            robot = _robot;
        }

        public void Rotate(bool left)
        {
            if (left)
                switch (robot.direction)
                {
                    case 'W':
                        robot.direction = 'S';
                        break;
                    case 'S':
                        robot.direction = 'E';
                        break;

                    case 'E':
                        robot.direction = 'N';
                        break;

                    case 'N':
                        robot.direction = 'W';
                        break;
                }
            else
                switch (robot.direction)
                {
                    case 'W':
                        robot.direction = 'N';
                        break;
                    case 'N':
                        robot.direction = 'E';
                        break;
                    case 'E':
                        robot.direction = 'S';
                        break;
                    case 'S':
                        robot.direction = 'W';
                        break;
                }
        }

        public void Move()
        {
            var isColision = CheckColision();

            switch (robot.direction)
            {
                case 'N':
                    if (!isColision)
                        robot.y++;
                    break;
                case 'S':
                    if (!isColision)
                        robot.y--;
                    break;
                case 'W':
                    if (!isColision)
                        robot.x--;
                    break;
                case 'E':
                    if (!isColision)
                        robot.x++;
                    break;
            }
        }

        public bool CheckColision()
        {
            if (robot.y == 0 && robot.direction == 'S')
            {
                robot.penalties++;
                return true;
            }
            else if (robot.y == board.height-1  && robot.direction == 'N')
            {
                robot.penalties++;
                return true;
            }
            else if (robot.x == 0 && robot.direction == 'W')
            {
                robot.penalties++;
                return true;
            }
            else if (robot.x == board.length -1 && robot.direction == 'E')
            { 
                robot.penalties++;
                return true;
            }
            return false;
        }


        public void UserInput(char input)
        {
            if (input == 'L')
                Rotate(true);
            else if (input == 'R')
                Rotate(false);
            else if (input == 'M')
                Move();
        }

        public string RobotDetails()
        {
            return "Position:" + robot.x + "," + robot.y + ","+robot.direction + " Penalties:" + robot.penalties;
        }

        public void SetUpInitialPosition(int x = 0, int y = 0, char direction = 'N')
        {
            robot.x =(x>0)?(x<4)? x: 4 : 0 ;
            robot.y = (y > 0) ? (y < 4) ? y : 4 : 0;
            robot.direction = direction;
        }

    }
}
