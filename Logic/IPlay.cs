﻿using Data;

namespace Logic
{
    public interface IPlay
    {
        IBoard board { get; set; }
        IRobot robot { get; set; }

        void Rotate(bool left);
        void Move();
        bool CheckColision();
        void UserInput(char input);
        string RobotDetails();
        void SetUpInitialPosition(int v1, int v2, char direction);
    }
}