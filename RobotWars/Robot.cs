﻿namespace Data
{
    public class Robot : IRobot
    {
        public int x { get; set; }
        public int y { get; set; }
        public int penalties { get; set; }
        public char direction { get; set; }

        public Robot()
        {

        }

    }
}