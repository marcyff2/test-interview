﻿namespace Data
{
    public enum eDirection
    {
        N = 1,
        S = 2,
        E = 3,
        W = 4
    }
}