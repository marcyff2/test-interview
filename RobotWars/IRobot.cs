﻿namespace Data
{
    public interface IRobot
    {
        int x { get; set; }
        int y { get; set; }
        int penalties { get; set; }
        char direction { get; set; }
    }
}