﻿namespace Data
{
    public class Board : IBoard
    {
        public int length { get; set; }
        public int height { get; set; }
        public Board(int _length, int _height)
        {
            length = _length;
            height = _height;
        }
    }
}