﻿namespace Data
{
    public interface IBoard
    {
        int length { get; set; }
        int height { get; set; }

    }
}
