﻿using Moq;
using Logic;
using Data;
using NUnit.Framework;


namespace Logic.Test
{
    [TestFixture]
    public class TestPlay
    {
        IPlay play;
        [SetUp]
        public void Initialise()
        {
            //IRobot robot = new Robot();
            var robot = new Mock<Robot>().As<IRobot>();
            robot.SetupAllProperties();
            var board = new Mock<IBoard>();
            board.Setup(t=>t.length).Returns(5);
            board.Setup(t=>t.height).Returns(5);
            play = new Play(robot.Object, board.Object);
        }
        //Solution to the Exercise

        #region Exercise
        [TestCase("MLMRMMMRMMRR", 0, 2, 'E', "Position:4,1,N Penalties:0")]
        public void TestCase1(string Input, int X, int Y, char Direction, string ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(X, Y, Direction);
            foreach (var _char in Input)
                play.UserInput(_char);
            Assert.AreEqual(ExpectedResult, play.RobotDetails());
        }
        [TestCase("LMLLMMLMMMRMM", 4, 4, 'S', "Position:0,1,W Penalties:1")]
        public void TestCase2(string Input, int X, int Y, char Direction, string ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(X, Y, Direction);
            foreach (var _char in Input)
                play.UserInput(_char);
            Assert.AreEqual(play.RobotDetails(), ExpectedResult);
        }
        [TestCase("MLMLMLM RMRMRMRM", 2, 2, 'W', "Position:2,2,N Penalties:0")]
        public void TestCase3(string Input, int X, int Y, char Direction, string ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(X, Y, Direction);
            foreach (var _char in Input)
                play.UserInput(_char);
            Assert.AreEqual(play.RobotDetails(), ExpectedResult);
        }

        [TestCase("MMLMMLMMMMM", 1, 3, 'N', "Position:0,0,S Penalties:3")]
        public void TestCase4(string Input, int X, int Y, char Direction, string ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(X, Y, Direction);
            foreach (var _char in Input)
                play.UserInput(_char);
            Assert.AreEqual(play.RobotDetails(), ExpectedResult);
        }
        #endregion


        // Code used to develop logic layer. For the solution to the project look above 
        #region TDD
        [TestCase(0,0,'N',"0,0,N")]
        [TestCase(2, 1, 'E', "2,1,E")]
        [TestCase(5, 5, 'E', "4,4,E")]

        public void PlaceRobotInBoard_GetLocation(int x, int y, char direction, string ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(x, y, direction);
            Assert.AreEqual(ExpectedResult, play.robot.x + ","+play.robot.y + ","+ play.robot.direction);
        }

        [TestCase('N', true, 'W')]
        [TestCase('E', true, 'N')]
        [TestCase('S', true, 'E')]
        [TestCase('W', true, 'S')]
        [TestCase('N', false, 'E')]
        [TestCase('W', false, 'N')]
        [TestCase('S', false, 'W')]
        [TestCase('E', false, 'S')]
        public void RotateRobot_Direction_Expect_NewDirection(char direction, bool left, char ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(0, 0, direction);
            play.Rotate(left);
            Assert.AreEqual(ExpectedResult, play.robot.direction);
        }

        [TestCase('S', 0, 0)]
        [TestCase('S', 1, 0)]
        [TestCase('N', 0, 1)]
        [TestCase('N', 4, 4)]
        public void MoveYAxis_WithYValueAndDirection_ExpectNewYValue(char direction, int Y, int ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(0, Y, direction);
            play.Move();
            Assert.AreEqual(ExpectedResult, play.robot.y);
        }
        [TestCase('W', 0, 0)]
        [TestCase('W', 1, 0)]
        [TestCase('E', 0, 1)]
        [TestCase('E', 4, 4)]
        public void MoveXAxis_WithXValueAndDirection_ExpectNewXValue(char direction, int X, int ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(X, 0, direction);
            play.Move();
            Assert.AreEqual(ExpectedResult, play.robot.x);
        }
        [TestCase('S', 0, 0, 1)]
        [TestCase('N', 0, 4, 1)]
        [TestCase('N', 0, 0, 0)]
        [TestCase('S', 0, 4, 0)]
        [TestCase('W', 0, 0, 1)]
        [TestCase('E', 4, 0, 1)]
        [TestCase('W', 4, 0, 0)]
        [TestCase('E', 0, 0, 0)]
        public void ColisionTesting_WithInitialValueAndDirection_ExpectPenaltiesChange(char direction, int X, int Y, int ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(X, Y, direction);

            play.CheckColision();
            Assert.AreEqual(ExpectedResult, play.robot.penalties);
        }

        [TestCase('L', "Position:0,0,E Penalties:0")]
        [TestCase('R', "Position:0,0,W Penalties:0")]
        [TestCase('M', "Position:0,0,S Penalties:1")]
        public void TestAction_WithSingleUserInput_ChangeInBoard(char Input, string ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(0, 0, 'S');
            play.UserInput(Input);
            Assert.AreEqual(ExpectedResult, play.RobotDetails());

        }
        [TestCase("LMRM", "Position:1,0,S Penalties:1")]
        public void TestAction_WithMultipleUserInput_ChangeInBoard(string Input, string ExpectedResult)
        {
            Initialise();
            play.SetUpInitialPosition(0, 0, 'S');
            foreach (var _char in Input)
                play.UserInput(_char);
            Assert.AreEqual(ExpectedResult, play.RobotDetails());
        }
        #endregion
    }
}
